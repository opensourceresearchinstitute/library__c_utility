

This directory is the top level for developing (modifying) the various static "helper" libraries used inside proto-runtime code, and also used by applications..

Each is developed and compiled as its own, independent, static library.  

The projects have two kinds of header file -- one for "external" use, which has all symbols visible to external code, and only those symbols, and the other, which has symbols used only internally.  Only the external header is copied to the include directory.

Each folder contains two different netbeans projects, one that builds the library as a normal static library, which is meant to be used by applications.  The other version compiles the code with the -PIC flag, so that the static library can in turn be used inside dynamically linked shared libraries, which require position-independent object files.  Both versions of each library must be copied to the lib directory.  They should be named the same, except the postion-indepedent ends with "_pic.a"

The libraries can be built by using netbeans.  Each directory contains two netbeans projects for 

These libraries may be obtained in two different ways..  either as part of a larger project, or independently, by themselves.  Depending on how they are obtained, the procedure for moving the files after building them is different.  

If included as part of a .zip file that has all the components of a larger project, then the ".a" files are copied to the PR__lib directory that was uncompressed from the .zip.  If the external ".h" was modified during the development, then the new version should be copied to the PR__include directory.  (However, if the library was gained as part of a larger project, then the library files may be edited while doing development inside a different netbeans project. In that case, that netbeans project will be pointed to the .h in the PR__include directory.  If it is modified, then the new .h must be copied from there back to this local directory.)

If obtained separately, the ".a" files should be copied to /usr/lib and the external .h copied to /usr/include/PR__include.




