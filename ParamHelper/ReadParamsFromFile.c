/* 
 * Author: SeanHalle@yahoo.com
 *
 * Created on June 15, 2009, 10:12 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <PR__include/prparam.h>
#include <PR__include/prmalloc.h>

ParamStruc * makeParamFromStrs( char * type, char *value );

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

/*Copied from gnu's win32 lib
 */
 ssize_t
getline( char **lineptr, size_t *n, FILE *stream )
 {
   if ( lineptr == NULL || n == NULL)  return -1;
   if (*lineptr == NULL || *n == 0)
    { *n = 500; //max length of line in a config file
      *lineptr = (char *) PR__malloc( *n );
      if (*lineptr == NULL) return -1;
    }
   if( fgets( *lineptr, *n, stream ) )
      return *n;
   else
      return -1;
 }

 void
readParamFileIntoBag( char *paramFileName, ParamBag * bag )
 {
   size_t lineSz = 0;
   FILE* paramFile;
   char* line = NULL;

   char* paramType;//  = malloc( 12 );  //"double" is the longest type
   char* paramName;//  = malloc( 500 ); //max of 500 chars in name
   char* paramValue;// = malloc( 500 ); //max of 500 chars in value
   
   lineSz = 500; //max length of line in a config file
   line = (char *) PR__malloc( lineSz );
   if( line == NULL )
    { printf( "\nIn readParamFileIntoBag: no mem for line\n" );
      return;
    }
   
   
   paramFile = fopen( paramFileName, "r" );
   if( paramFile == NULL ) 
    { printf("\ncouldn't open param file: %s\n", paramFileName); exit(0); }
   fseek( paramFile, 0, SEEK_SET );
   while( !feof( paramFile ) )
    { while( getline( &line, &lineSz, paramFile ) != -1 )
       {
         char *lineEnd = line + strlen(line) +1;
         char *searchPos = line;
         
         if( *line == '\n') continue; //blank line
         if( *line == '/' ) continue; //comment line

            //read the param type
         paramType = line; //start of string
         int foundIt = 0;
         for( ; searchPos < lineEnd && !foundIt; searchPos++)
          { if( *searchPos == ',' )
             { foundIt = 1;
               *searchPos = 0; //mark end of string
             }
          }
            //get rid of leading spaces
         for( ; searchPos < lineEnd; searchPos++)
          { if( *searchPos != ' ' ) break;
          }
            //read the param name
         paramName = searchPos;
         foundIt = 0;
         for( ; searchPos < lineEnd && !foundIt; searchPos++)
          { if( *searchPos == ',' )
             { foundIt = 1;
               *searchPos = 0; //mark end
             }
          }
            //get rid of leading spaces
         for( ; searchPos < lineEnd; searchPos++)
          { if( *searchPos != ' ' ) break;
          }
            //read the param value
         paramValue = searchPos;
         foundIt = 0;
         for( ; searchPos < lineEnd && !foundIt; searchPos++)
          { if( *searchPos == '\n' )
             { foundIt = 1;
               *searchPos = 0; //mark end
             }
          }
         if( foundIt )
          { printf("Found: %s, %s, %s\n", paramType, paramName, paramValue );
             ParamStruc *
            paramStruc = makeParamFromStrs( paramType, paramValue );
            addParamToBag( paramName, paramStruc, bag );
          }
       }
    }
   PR__free( line );
 }

