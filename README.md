OpenSourceResearchInstitute.org   Utility Libraries

This is a set of utility libraries written in C.
The utilities include:
Hash table
Queue  -- multi-threaded safe, as well as simple non-thread safe
and so on (aso)

They all have cmake, as well as a netbeans project directory

They are all static libraries, with a postion-independent-code (PIC) version and a normal version

To compile them, you can just cd into the root directory and type:
cmake .
make

This will do an in-source compile.  (Although, the cmake is designed to move the libraries to a location that is specific to the proto-runtime development environment.  You will likely want to change the cmake so that it moves the built .a files to a location of your choice)

The cmake is designed to be included in a larger project.  In that case, there will be a higher level cmake file in a directory above this one, just add a line to that to include this sub-directory.  Then modify the cmake files in here so that they move their .a files to a location of your choice.

For questions, email seanhalle@opensourceresearchinstitute.org