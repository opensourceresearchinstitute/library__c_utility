## Variables for CPack Debian packaging

set(CPACK_PACKAGE_DESCRIPTION "Utility Libraries")
include(CPack)

cpack_add_component(lib_util_c-dev
                     DISPLAY_NAME libraries__C_utility-dev
                     DESCRIPTION This is a set of include files for development with utility libraries written in C
                     HIDDEN
                     DEPENDS lib_util_c
                     DOWNLOADED)

cpack_add_component(lib_util_c
                     DISPLAY_NAME libraries__C_utility
                     DESCRIPTION This is a set of utility libraries written in C
                     REQUIRED
                     DOWNLOADED)
