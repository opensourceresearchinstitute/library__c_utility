cmake_minimum_required(VERSION 2.8)
message("Queue_impl")

#This is the wrong way to force using gcc, but need this file to run 
# stand alone -- IE, there is no makefile to set environment variables 
set(CMAKE_C_COMPILER "/usr/bin/gcc")

#This is a library that can be compiled on its own, and lives in a 
# separate repository.  It can be independently developed from the rest
# of the top level project
#This file can be run stand-alone, or as part of the top level project,
# if run stand-alone, it won't have flags defined via the top level project

#for debugging compile -- causes makefile to output full compile cmd line
#set(CMAKE_VERBOSE_MAKEFILE "1")

#Check if the top level project directories are set.  Normally they are set
# by the top level cmake file.  If not, then running stand alone. Set them.
if(NOT PRTOPLEVEL_INCLUDE_DIR)
   set(PRTOPLEVEL_INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../dist/include)
endif(NOT PRTOPLEVEL_INCLUDE_DIR)
if(NOT PRTOPLEVEL_LIB_DIR)
   set(PRTOPLEVEL_LIB_DIR "${CMAKE_CURRENT_SOURCE_DIR}/../../../dist/lib")
endif(NOT PRTOPLEVEL_LIB_DIR)
if(NOT PRTOPLEVEL_BIN_DIR)
   set(PRTOPLEVEL_BIN_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../dist/bin)
endif(NOT PRTOPLEVEL_BIN_DIR)

#where to look for dependent libraries to be linked to
link_directories("${PRTOPLEVEL_LIB_DIR}")

include_directories("${PRTOPLEVEL_INCLUDE_DIR}")

#This command tells cmake to create a static library, using the listed
# source files.  Commands below will tell cmake where to move the compiled
# library to
set(SOURCES_LIST
  BlockingQueue.c
  PrivateQueue.c
)

#this actually builds the library file, using the SOURCES_LIST that was
# built up from all the sub-directories plus above
add_library(prqueue_pic STATIC ${SOURCES_LIST} )
#add flag to create a position-independent-code version of the library
set_target_properties (prqueue_pic PROPERTIES COMPILE_FLAGS -fPIC)

add_library(prqueue STATIC ${SOURCES_LIST} )

#sets where to move the built library files to
set_target_properties(prqueue_pic PROPERTIES
         ARCHIVE_OUTPUT_DIRECTORY  "${PRTOPLEVEL_LIB_DIR}")
set_target_properties(prqueue PROPERTIES
         ARCHIVE_OUTPUT_DIRECTORY  "${PRTOPLEVEL_LIB_DIR}")

#This command says which libraries are linked to by what is built by this
# file (this line is copy-pasted into many places, so could be executable
# or library, etc, being built in this cmake lists file)
#The first argument is the name defined by the "add_library" command
#Just say the name of the needed library, as it appears in the lib 
# directory, without the extension.  LINK_PUBLIC is internal to cmake, tells it
# to populate both PRIVATE and INTERFACE modes of properties.
#Looks in "link_directories" directory for these 
target_link_libraries (prqueue LINK_PUBLIC prmalloc)
target_link_libraries (prqueue_pic LINK_PUBLIC prmalloc)
LINK_LIBRARIES(${CMAKE_THREAD_LIBS} ${CMAKE_DL_LIBS})
find_package (Threads)
link_libraries (${CMAKE_THREAD_LIBS_INIT})

#setting -O3, which conflicts it -g.. it breaks debug single step
set(CMAKE_C_FLAGS
       "${CMAKE_FLAGS} -c -g -MMD -MP -O3 -D__STDC_FORMAT_MACROS -D__FILENAME__='\"$(subst ${CMAKE_SOURCE_DIR}/,,$(abspath $<))\"'")

install(TARGETS prqueue_pic prqueue ARCHIVE DESTINATION ${LIBRARY_OUTPUT_PATH} COMPONENT lib_util_c)
install(FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/BlockingQueue.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PrivateQueue.h
        ${PRTOPLEVEL_INCLUDE_DIR}/PR__include/prqueue.h
            DESTINATION ${CMAKE_INCLUDE_PATH} COMPONENT lib_util_c-dev)
