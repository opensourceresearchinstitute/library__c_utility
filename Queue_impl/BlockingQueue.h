/* 
 * File:   BlockingQueue.h
 * Author: SeanHalle@yahoo.com
 *
 * Created on November 11, 2009, 12:51 PM
 */

#ifndef  _BLOCKINGQUEUE_H
#define	_BLOCKINGQUEUE_H

#include <PR__include/PR__primitive_data_types.h>
#include <PR__include/prmalloc.h>


#define TRUE  1
#define FALSE 0

#define LOCKED   1
#define UNLOCKED 0

#endif	/* _BLOCKINGQUEUE_H */

