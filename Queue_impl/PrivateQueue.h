/*
 *  Copyright 2009 OpenSourceResearchInstitute.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 */

#ifndef _PRIVATE_QUEUE_H
#define	_PRIVATE_QUEUE_H

#include <PR__include/PR__primitive_data_types.h>

#define TRUE     1
#define FALSE    0

#define LOCKED   1
#define UNLOCKED 0


#endif	/* _PRIVATE_QUEUE_H */

