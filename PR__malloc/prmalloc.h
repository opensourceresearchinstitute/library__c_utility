/*
 *  Copyright 2009 OpenSourceCodeStewardshipFoundation.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 * Created on November 14, 2009, 9:07 PM
 */

#ifndef _PRMALLOC_H
#define	_PRMALLOC_H

#include <malloc.h>
#include <inttypes.h>
#include <math.h>
#include <PR__include/PR__primitive_data_types.h>

void *
PR_int__malloc( size_t sizeRequested );

void *
PR_int__malloc_aligned( size_t sizeRequested );

void
PR_int__free( void *ptrToFree );

//Use these in application code directly
#define \
PR__malloc  PR_WL__malloc

#define \
PR__free    PR_WL__free

//these are implemented in the PR leaf implementation
void *
PR_WL__malloc( int32  sizeRequested ); 

void
PR_WL__free( void *ptrToFree );


/*Allocates memory from the external system -- higher overhead
 */
void *
PR_ext__malloc_in_ext( size_t sizeRequested );

/*Frees memory that was allocated in the external system -- higher overhead
 */
void
PR_ext__free_in_ext( void *ptrToFree );

#endif