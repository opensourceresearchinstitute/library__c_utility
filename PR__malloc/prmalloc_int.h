/*
 *  Copyright 2009 OpenSourceCodeStewardshipFoundation.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 * Created on November 14, 2009, 9:07 PM
 */

#ifndef _PRMALLOC_INT_H
#define	_PRMALLOC_INT_H

#include <malloc.h>
#include <inttypes.h>
#include <math.h>
#include <PR__include/PR__primitive_data_types.h>

   // memory for PR_int__malloc
#define MALLOC_ADDITIONAL_MEM_FROM_OS_SIZE 0x8000000 /* 128M */
#define PAGE_SIZE     4096


#define SMALL_CHUNK_SIZE 32
#define SMALL_CHUNK_COUNT 4
#define LOWER_BOUND     128  //Biggest chunk size that is created for the small chunks
#define BIG_LOWER_BOUND 160  //Smallest chunk size that is created for the big chunks

#define LOG54 0.3219280948873623
#define LOG128 7


#define MAX_UINT64 0xFFFFFFFFFFFFFFFF



typedef struct _MallocProlog MallocProlog;

struct _MallocProlog
 {
   MallocProlog *nextChunkInFreeList;
   MallocProlog *prevChunkInFreeList;
   MallocProlog *nextHigherInMem;
   MallocProlog *nextLowerInMem;
 };
//MallocProlog
 
 typedef struct MallocArrays MallocArrays;

 struct MallocArrays
 {
     MallocProlog **smallChunks;
     MallocProlog **bigChunks;
     uint64       bigChunksSearchVector[2];
     void         *memSpace;
     uint32       containerCount;
 };
 //MallocArrays

typedef struct
 {
   MallocProlog *firstChunkInFreeList;
   int32         numInList; //TODO not used
 }
FreeListHead;


//=============== Initializer and Finalizer ===============
__attribute__((constructor)) 
void 
_PR_malloc_init();

__attribute__((destructor)) 
void
_PR_malloc_finalize();

void *
PR_int__malloc( size_t sizeRequested );

void *
PR_WL__malloc( int32  sizeRequested ); /*BUG: -- get master lock */

void *
PR_int__malloc_aligned( size_t sizeRequested );

void
PR_int__free( void *ptrToFree );

void
PR_WL__free( void *ptrToFree );



/*Allocates memory from the external system -- higher overhead
 */
void *
PR_ext__malloc_in_ext( size_t sizeRequested );

/*Frees memory that was allocated in the external system -- higher overhead
 */
void
PR_ext__free_in_ext( void *ptrToFree );


MallocArrays *
PR_ext__create_free_list();

void
PR_ext__free_free_list(MallocArrays *freeLists );

#endif