/*
 *  Copyright 2009 OpenSourceCodeStewardshipFoundation.org
 *  Licensed under GNU General Public License version 2
 *
 * Author: seanhalle@yahoo.com
 *
 * Created on November 14, 2009, 9:07 PM
 */

#include <malloc.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <math.h>


#include "prmalloc_int.h"


/*
 */
//===============  Global vars for library  ===============
MallocArrays *mallocGlobalFreeLists;
int           mallocGlobalAmtOfOutstandingMem;

//=============== Initializer and Finalizer ===============
__attribute__((constructor)) 
void 
_PR_malloc_init()
 {
   mallocGlobalFreeLists    = PR_ext__create_free_list();
   mallocGlobalAmtOfOutstandingMem = 0;
   
   printf("PR__malloc_initialized\n");
 }

__attribute__((destructor)) 
void
_PR_malloc_finalize()
 {
    PR_ext__free_free_list( mallocGlobalFreeLists );
 }

//=========================================================
//A MallocProlog is a head element if the HigherInMem variable is NULL
//A Chunk is free if the prevChunkInFreeList variable is NULL

/*
 * This calculates the container which fits the given size.
 */
inline
uint32 getContainer(size_t size)
{
    return (log2(size)-LOG128)/LOG54;
}

/*
 * Removes the first chunk of a freeList
 * The chunk is removed but not set as free. There is no check if
 * the free list is empty, so make sure this is not the case.
 */
inline
MallocProlog *removeChunk(MallocArrays* freeLists, uint32 containerIdx)
{
    MallocProlog** container = &freeLists->bigChunks[containerIdx];
    MallocProlog*  removedChunk = *container;
    *container = removedChunk->nextChunkInFreeList;
    
    if(removedChunk->nextChunkInFreeList)
        removedChunk->nextChunkInFreeList->prevChunkInFreeList = 
                (MallocProlog*)container;
    
    if(*container == NULL)
    {
       if(containerIdx < 64)
           freeLists->bigChunksSearchVector[0] &= ~((uint64)1 << containerIdx); 
       else
           freeLists->bigChunksSearchVector[1] &= ~((uint64)1 << (containerIdx-64));
    }
    
    return removedChunk;
}

/*
 * Removes the first chunk of a freeList
 * The chunk is removed but not set as free. There is no check if
 * the free list is empty, so make sure this is not the case.
 */
inline
MallocProlog *removeSmallChunk(MallocArrays* freeLists, uint32 containerIdx)
{
    MallocProlog** container = &freeLists->smallChunks[containerIdx];
    MallocProlog*  removedChunk = *container;
    *container = removedChunk->nextChunkInFreeList;
    
    if(removedChunk->nextChunkInFreeList)
        removedChunk->nextChunkInFreeList->prevChunkInFreeList = 
                (MallocProlog*)container;
    
    return removedChunk;
}

inline
size_t getChunkSize(MallocProlog* chunk)
{
    return (uintptr_t)chunk->nextHigherInMem -
            (uintptr_t)chunk - sizeof(MallocProlog);
}

/*
 * Removes a chunk from a free list.
 */
inline
void extractChunk(MallocProlog* chunk, MallocArrays *freeLists)
{
   chunk->prevChunkInFreeList->nextChunkInFreeList = chunk->nextChunkInFreeList;
   if(chunk->nextChunkInFreeList)
       chunk->nextChunkInFreeList->prevChunkInFreeList = chunk->prevChunkInFreeList;
   
   //The last element in the list points to the container. If the container points
   //to NULL the container is empty
   if(*((void**)(chunk->prevChunkInFreeList)) == NULL && getChunkSize(chunk) >= BIG_LOWER_BOUND)
   {
       //Find the approppiate container because we do not know it
       uint64 containerIdx = ((uintptr_t)chunk->prevChunkInFreeList - (uintptr_t)freeLists->bigChunks) >> 3;
       if(containerIdx < (uint32)64)
           freeLists->bigChunksSearchVector[0] &= ~((uint64)1 << containerIdx); 
       if(containerIdx < 128 && containerIdx >=64)
           freeLists->bigChunksSearchVector[1] &= ~((uint64)1 << (containerIdx-64)); 
       
   }
}

/*
 * Merges two chunks.
 * Chunk A has to be before chunk B in memory. Both have to be removed from
 * a free list
 */
inline
MallocProlog *mergeChunks(MallocProlog* chunkA, MallocProlog* chunkB)
{
    chunkA->nextHigherInMem = chunkB->nextHigherInMem;
    chunkB->nextHigherInMem->nextLowerInMem = chunkA;
    return chunkA;
}
/*
 * Inserts a chunk into a free list.
 */
inline
void insertChunk(MallocProlog* chunk, MallocProlog** container)
{
    chunk->nextChunkInFreeList = *container;
    chunk->prevChunkInFreeList = (MallocProlog*)container;
    if(*container)
        (*container)->prevChunkInFreeList = chunk;
    *container = chunk;
}

/*
 * Divides the chunk that a new chunk of newSize is created.
 * There is no size check, so make sure the size value is valid.
 */
inline
MallocProlog *divideChunk(MallocProlog* chunk, size_t newSize)
{
    MallocProlog* newChunk = (MallocProlog*)((uintptr_t)chunk->nextHigherInMem -
            newSize - sizeof(MallocProlog));
    
    newChunk->nextLowerInMem  = chunk;
    newChunk->nextHigherInMem = chunk->nextHigherInMem;
    
    chunk->nextHigherInMem->nextLowerInMem = newChunk;
    chunk->nextHigherInMem = newChunk;
    
    return newChunk;
}

/* 
 * Search for chunk in the list of big chunks. Split the block if it's too big
 */
inline
MallocProlog *searchChunk(MallocArrays *freeLists, size_t sizeRequested, uint32 containerIdx)
{
    MallocProlog* foundChunk;
    
    uint64 searchVector = freeLists->bigChunksSearchVector[0];
    //set small chunk bits to zero
    searchVector &= MAX_UINT64 << containerIdx;
    containerIdx = __builtin_ffsl(searchVector); //least significant 1 bit

    if(containerIdx == 0)
    {
       searchVector = freeLists->bigChunksSearchVector[1];
       containerIdx = __builtin_ffsl(searchVector);
       if(containerIdx == 0)
       {
           //TODO: get additional mem and insert into free list
           //malloc( MALLOC_ADDITIONAL_MEM_FROM_OS_SIZE );
           printf("PR malloc failed: low memory");
           exit(1);   
       }
       containerIdx += 64;
    }
    containerIdx--;
    

    foundChunk = removeChunk(freeLists, containerIdx);
    size_t chunkSize     = getChunkSize(foundChunk);

    //If the new chunk is larger than the requested size: split
    if(chunkSize > sizeRequested + 2 * sizeof(MallocProlog) + BIG_LOWER_BOUND)
    {
       MallocProlog *newChunk = divideChunk(foundChunk,sizeRequested);
       containerIdx = getContainer(getChunkSize(foundChunk)) - 1;
       insertChunk(foundChunk,&freeLists->bigChunks[containerIdx]);
       if(containerIdx < 64)
           freeLists->bigChunksSearchVector[0] |= ((uint64)1 << containerIdx);
       else
           freeLists->bigChunksSearchVector[1] |= ((uint64)1 << (containerIdx-64));
       foundChunk = newChunk;
    } 
    
    return foundChunk;
}


/*
 * This is sequential code, meant to only be called from the Master, not from
 * any slave Slvs.
 * 
 *May 2012
 *ToDo: Improve speed, by using built-in leading 1 detector to calc free-list
 * index.
 *Change to two separate arrays, one for free-lists of small fixed-size chunks
 * other for free lists of exponentially growing chunk sizes
 *Do simple compare to decide which array of lists to use
 *For small chunks, size the lists in increments of 16, up to, say, 128 (1024
 * is max if want less than 64 lists, which allows searching for first
 * occupied free-list using leading-1 detector on a bit-vector)
 *To find index, right-shift by 4 bits, and that's the index! (works because
 * compare says no 1's above 128 position ((bit 7)), and sizes are every 16,
 * so dividing by 16 equals exactly the position)
 *For large chunks, have 63 free lists, but split into even and odd indexes.
 *For even indexes, each list starts with chunks twice the size of previous
 * even index.
 *For odd indexes, each list starts with chunks of size half-way between those
 * of the even indexes on either side.
 *
 *To calc the free-list position of a requested size, get pos of leading 1
 * of the size, call this msbsP (most-significant-bit-set-position). Then
 * check bit to right of it (one-less-significant)
 *If it's 0 then use the even index: msbsP * 2, which is msbsP << 1.
 *If it's 1, then use the odd-index, which is msbsP << 1  + 1
 *
 *To find msbsP, use GCC builtin: "int __builtin_clzll (unsigned long long)"
 * which returns the number of zeros above (left of) msb set.  Note, dies if
 * give it zero, but the compare used to choose between arrays makes sure
 * requested size given to it is not zero.
 * 
 *This scheme keeps wastage small, while finding free element is O(1), and a
 * fast constant.
 *For large chunk sizes, if don't shave excess, then it ensures worst-case
 * wastage due to mis-match in size of chunk vs requested size is 33% 
 * (invariant: take any even list.. it starts at a power of 2, and next list
 *  up starts at 50% larger, so biggest chunk is 1.5 x smallest request, that's
 *  33% of total memory wasted. Then, for the odd index above, smallest chunk
 *  is 2x for smallest request of 1.5x, for 25% total wasted memory)
 *For smallest size chunks, the pre-amble wastes quite a bit, but above that,
 * sizing in increments of 16 keeps wastage small.  And, if always shave, then
 * wastage due to size mis-match is maximum 16 bytes for the large chunks.
 * 
 */
void *
PR_int__malloc( size_t sizeRequested )
 {     
//   return malloc( sizeRequested );
    
   MallocArrays* freeLists = mallocGlobalFreeLists;
   MallocProlog* foundChunk;
   
   //Return a small chunk if the requested size is smaller than 128B
   if(sizeRequested <= LOWER_BOUND)
    {
      uint32 freeListIdx = (sizeRequested-1)/SMALL_CHUNK_SIZE;
      if(freeLists->smallChunks[freeListIdx] == NULL)
        foundChunk = searchChunk(freeLists, SMALL_CHUNK_SIZE*(freeListIdx+1), 0);
      else
        foundChunk = removeSmallChunk(freeLists, freeListIdx);
       
      //Mark as allocated
      foundChunk->prevChunkInFreeList = NULL;      
      return foundChunk + 1;
    }
   
   //Calculate the expected container. Start one higher to have a Chunk that's
   //always big enough.
   uint32 containerIdx = getContainer(sizeRequested);
   
   if(freeLists->bigChunks[containerIdx] == NULL)
       foundChunk = searchChunk(freeLists, sizeRequested, containerIdx); 
   else
       foundChunk = removeChunk(freeLists, containerIdx); 
   
   //Mark as allocated
   foundChunk->prevChunkInFreeList = NULL;      
      
   //skip over the prolog by adding its size to the pointer return
   return foundChunk + 1;
 }



/*
 * This is sequential code, meant to only be called from the Master, not from
 * any slave Slvs.
 */
void
PR_int__free( void *ptrToFree )
 {
//   free( ptrToFree );
            
   MallocArrays* freeLists = mallocGlobalFreeLists;
   MallocProlog *chunkToFree = (MallocProlog*)ptrToFree - 1;
   uint32 containerIdx;
   
   //Check for free neighbors
   if(chunkToFree->nextLowerInMem)
   {
       if(chunkToFree->nextLowerInMem->prevChunkInFreeList != NULL)
       {//Chunk is not allocated
           extractChunk(chunkToFree->nextLowerInMem, freeLists);
           chunkToFree = mergeChunks(chunkToFree->nextLowerInMem, chunkToFree);
       }
   }
   if(chunkToFree->nextHigherInMem)
   {
       if(chunkToFree->nextHigherInMem->prevChunkInFreeList != NULL)
       {//Chunk is not allocated
           extractChunk(chunkToFree->nextHigherInMem, freeLists);
           chunkToFree = mergeChunks(chunkToFree, chunkToFree->nextHigherInMem);
       }
   }
   
   size_t chunkSize = getChunkSize(chunkToFree);
   if(chunkSize < BIG_LOWER_BOUND)
   {
       containerIdx =  (chunkSize/SMALL_CHUNK_SIZE)-1;
       if(containerIdx > SMALL_CHUNK_COUNT-1)
           containerIdx = SMALL_CHUNK_COUNT-1;
       insertChunk(chunkToFree, &freeLists->smallChunks[containerIdx]);
   }
   else
   {
       containerIdx = getContainer(getChunkSize(chunkToFree)) - 1;
       insertChunk(chunkToFree, &freeLists->bigChunks[containerIdx]);
       if(containerIdx < 64)
           freeLists->bigChunksSearchVector[0] |= (uint64)1 << containerIdx;
       else
           freeLists->bigChunksSearchVector[1] |= (uint64)1 << (containerIdx-64);
   }   
 }


/*
 * Designed to be called from the main thread outside of PR, during init
 */
MallocArrays *
PR_ext__create_free_list()
{     
   //Initialize containers for small chunks and fill with zeros
   MallocArrays *freeLists = (MallocArrays*)malloc( sizeof(MallocArrays) );
   
   freeLists->smallChunks = 
           (MallocProlog**)malloc(SMALL_CHUNK_COUNT*sizeof(MallocProlog*));
   memset((void*)freeLists->smallChunks,
           0,SMALL_CHUNK_COUNT*sizeof(MallocProlog*));
   
   //Calculate number of containers for big chunks
   uint32 container = getContainer(MALLOC_ADDITIONAL_MEM_FROM_OS_SIZE)+1;
   freeLists->bigChunks = (MallocProlog**)malloc(container*sizeof(MallocProlog*));
   memset((void*)freeLists->bigChunks,0,container*sizeof(MallocProlog*));
   freeLists->containerCount = container;
   
   //Create first element in lastContainer 
   MallocProlog *firstChunk = malloc( MALLOC_ADDITIONAL_MEM_FROM_OS_SIZE );
   if( firstChunk == NULL ) {printf("Can't allocate initial memory\n"); exit(1);}
   freeLists->memSpace = firstChunk;
   
   //Touch memory to avoid page faults
   void *ptr,*endPtr; 
   endPtr = (void*)firstChunk+MALLOC_ADDITIONAL_MEM_FROM_OS_SIZE;
   for(ptr = firstChunk; ptr < endPtr; ptr+=PAGE_SIZE)
   {
       *(char*)ptr = 0;
   }
   
   firstChunk->nextLowerInMem = NULL;
   firstChunk->nextHigherInMem = (MallocProlog*)((uintptr_t)firstChunk +
                        MALLOC_ADDITIONAL_MEM_FROM_OS_SIZE - sizeof(MallocProlog));
   firstChunk->nextChunkInFreeList = NULL;
   //previous element in the queue is the container
   firstChunk->prevChunkInFreeList = &freeLists->bigChunks[container-2];
   
   freeLists->bigChunks[container-2] = firstChunk;
   //Insert into bit search list
   if(container <= 65)
   {
       freeLists->bigChunksSearchVector[0] = ((uint64)1 << (container-2));
       freeLists->bigChunksSearchVector[1] = 0;
   }   
   else
   {
       freeLists->bigChunksSearchVector[0] = 0;
       freeLists->bigChunksSearchVector[1] = ((uint64)1 << (container-66));
   }
   
   //Create dummy chunk to mark the top of stack this is of course
   //never freed
   MallocProlog *dummyChunk = firstChunk->nextHigherInMem;
   dummyChunk->nextHigherInMem = dummyChunk+1;
   dummyChunk->nextLowerInMem  = NULL;
   dummyChunk->nextChunkInFreeList = NULL;
   dummyChunk->prevChunkInFreeList = NULL;
   
   return freeLists;
 }


/*Designed to be called from the main thread outside of PR, during cleanup
 */
void
PR_ext__free_free_list( MallocArrays *freeLists )
 {    
   free(freeLists->memSpace);
   free(freeLists->bigChunks);
   free(freeLists->smallChunks);
   
 }

