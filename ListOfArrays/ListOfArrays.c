
#include <PR__include/prlistofarrays.h>
#include <PR__include/prmalloc.h>

ListOfArrays* makeListOfArrays(size_t entry_size, int num_entries_per_block){
    ListOfArrays* newLoA = (ListOfArrays*) PR__malloc(sizeof(ListOfArrays));
    newLoA->dim1info = makePrivDynArrayOfSize(&(newLoA->dim1),8);
       
    newLoA->entry_size = entry_size;
    newLoA->num_entries_per_fragment = num_entries_per_block;
    newLoA->next_free_index = 0;
    
    return newLoA;
}

void forAllInListOfArraysDo(ListOfArrays* list, ListOfArraysFnPtr fnPtr){
    int n;
    uintptr_t p;
    int num_full = list->next_free_index / list->num_entries_per_fragment; //list->dim1info->numInArray -1
    for(n=0;n<num_full; n++){
        for(p = (uintptr_t) (list->dim1)[n]; p < (uintptr_t) (list->dim1)[n] + list->num_entries_per_fragment * list->entry_size; p += list->entry_size){
            (*fnPtr)((void*)p);
        }
    }
    int offset_in_last = list->next_free_index % list->num_entries_per_fragment;
    n = list->dim1info->numInArray - 1;
    if (n >= 0){
        for(p = (uintptr_t)(list->dim1)[n]; p < (uintptr_t)(list->dim1)[n] + offset_in_last * list->entry_size; p += list->entry_size){
            (*fnPtr)((void*)p);
        }
    }
}

void freeListOfArrays(ListOfArrays* list){
    freeDynArrayDeep(list->dim1info,&PR_WL__free);
    PR_WL__free((void*)list);
}

/*
void addToListOfArraysDependency(Dependency value, ListOfArrays* list){
    int offset_in_fragment = list->next_free_index % list->num_entries_per_fragment; 
    if(offset_in_fragment == 0){ 
        void* newBlock = PR__malloc(list->entry_size * list->num_entries_per_fragment); 
        addToDynArray(newBlock,list->dim1info); 
    } 
    Dependency* typedFragment = (Dependency*) ((list->dim1)[list->dim1info->numInArray -1]); 
    typedFragment[offset_in_fragment] = value; 
    list->next_free_index++; 
}

void addToListOfArraysUnit(Unit value, ListOfArrays* list){
    int offset_in_fragment = list->next_free_index % list->num_entries_per_fragment; 
    if(offset_in_fragment == 0){ 
        void* newBlock = PR__malloc(list->entry_size * list->num_entries_per_fragment); 
        addToDynArray(newBlock,list->dim1info); 
    } 
    Unit* typedFragment = (Unit*) ((list->dim1)[list->dim1info->numInArray -1]); 
    typedFragment[offset_in_fragment] = value; 
    list->next_free_index++; 
}
 */
 