/* 
 * File:   ListOfArrays.h
 * Author: Nina Engelhardt
 *
 * Created on December 16, 2011, 2:06 PM
 */

#ifndef  _LISTOFARRAYS_H
#define	_LISTOFARRAYS_H

#include<stddef.h>
#include <inttypes.h>

#include <PR__include/PR__primitive_data_types.h>
#include <PR__include/prdynarray.h>


typedef struct {
    void* next;
    void* data;
} ArrayFragment;

typedef struct {
    void** dim1;
    PrivDynArrayInfo* dim1info;
    //ArrayFragment* last;
    size_t entry_size;
    int num_entries_per_fragment;
    int next_free_index;
} ListOfArrays;

ListOfArrays* makeListOfArrays(size_t entry_size, int num_entries_per_block);

#define addToListOfArrays(type,value,list) do { \
    int offset_in_fragment = list->next_free_index % list->num_entries_per_fragment; \
    if(offset_in_fragment == 0){ \
        void* newBlock = PR__malloc(list->entry_size * list->num_entries_per_fragment); \
        addToDynArray(newBlock,list->dim1info); \
    } \
    type* typedFragment = (type*) ((list->dim1)[list->dim1info->numInArray -1]); \
    typedFragment[offset_in_fragment] = value; \
    list->next_free_index++; \
} while (0)

#define addToListOfArrays_ext(type,value,list) do { \
    int offset_in_fragment = list->next_free_index % list->num_entries_per_fragment; \
    if(offset_in_fragment == 0){ \
        void* newBlock = malloc(list->entry_size * list->num_entries_per_fragment); \
        addToDynArray(newBlock,list->dim1info); \
    } \
    type* typedFragment = (type*) ((list->dim1)[list->dim1info->numInArray -1]); \
    typedFragment[offset_in_fragment] = value; \
    list->next_free_index++; \
} while (0)

typedef void  (*ListOfArraysFnPtr)  ( void * );  //fn has to cast void *

void forAllInListOfArraysDo(ListOfArrays* list, ListOfArraysFnPtr fnPtr);

#define valueInListOfArrays(type,index,list) ((type*)((list->dim1)[index / list->num_entries_per_fragment]))[index % list->num_entries_per_fragment]

#define setValueInListOfArrays(type,index,value,list) ((type*)((list->dim1)[index / list->num_entries_per_fragment]))[index % list->num_entries_per_fragment] = value

void freeListOfArrays(ListOfArrays* list);

#endif	/* LISTOFARRAYS_H */

